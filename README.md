# Projeto desenvolvido para aplicação na vaga de estágio da Volvo.
-----------------------------------------------------------------

O projeto consite em desenvolver um script em python que manipula e extrai informações de dados do INPC através de uma planilha csv.

**Atenção**

Para funcionamento correto do programa as seguinte bibliotecas devem estar instaladas:
- pandas - https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html
- matplotlib - https://matplotlib.org/3.1.1/users/installing.html

A planilha que será acessada também deve te sido baixada e deve estar na mesma pasta que o programa.

**Fontes dos dados das planilhas:**
- https://biblioteca.ibge.gov.br/index.php/biblioteca-catalogo?view=detalhes&id=7236
- https://sidra.ibge.gov.br/tabela/1100