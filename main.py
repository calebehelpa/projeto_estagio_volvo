import pandas as pd
import matplotlib.pyplot as plt


def grafPlot(df, y_index, kind_plot, title_plot):
    """ Modo de chamar a funcao: grafPlot(DataFrame, coluna(s) que sera(ao) plotada(s), tipo de grafico, titulo do grafico ) """
    df.plot(kind=kind_plot, use_index=True, y=y_index, fontsize=5, title=title_plot, rot=0, figsize=(11,6))
    """ Quando a opcao line é usada em kind gera um warning devido a um bug, mais informacoes em: https://github.com/pandas-dev/pandas/issues/35684 """
    plt.savefig(title_plot+'.png', dpi=300)


def filterDataFrame(df, *args, **kwargs):
    """ Modo de chamar a funcao: filterDataFrame(DataFrame, ano1, ano2, ano3, ..., month1='mes1', month2='mes2', ...) """
    month = kwargs.get("month1")
    """ Testa se o month1 foi setado """
    if month != None:
        dfFilterd = df.filter(like=month+'/'+str(args[0]), axis=0)
        """ Deve-se primeiro fazer a atribuicao para o dfFilterd ter valores """
        i = 1
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            """ Com dfFilterd tendo valores podemos dar append, sempre lembrar que append nao e uma funcao inplace """
            i += 1
    else:
        dfFilterd = df.filter(like=str(args[0]), axis=0)
        """ Se nenhum month foi setado, podemos realizar a filtragem apenas pelos anos """
        i = 1
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=str(args[i]), axis=0))
            i += 1
        return dfFilterd

    """ Proximas linhas apenas relicam o que foi feito com month1 com os proximos months """

    month = kwargs.get("month2")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd

    """ Caso o month2 nao seja setado entendemos que nao ha mais months para filtragem """
    """ O mesmo se aplica para os proximos months """    

    month = kwargs.get("month3")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd
    
    month = kwargs.get("month4")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd

    month = kwargs.get("month5")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd
    
    month = kwargs.get("month6")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd

    month = kwargs.get("month7")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd
    
    month = kwargs.get("month8")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd
    
    month = kwargs.get("month9")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd

    month = kwargs.get("month10")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    else:
        return dfFilterd
    
    month = kwargs.get("month11")
    if month != None:
        i = 0
        while i <= len(args)-1:
            dfFilterd = dfFilterd.append(df.filter(like=month+'/'+str(args[i]), axis=0))
            i += 1
    return dfFilterd

def calculateAverage(df):
    """ Modo de chamar a funcao: calculateAverage(DataFrame) """
    num_rows = len(df.index)
    """ num_rows recebe o numero de linhas que serao somadas """
    dfAverage = df.astype(float).sum(axis=0)
    """ Deve ser feito o casting para correto funcionamento da funcao """
    return dfAverage.div(num_rows)
    """ ATENCAO o dataFrame devolvido possui agora como index de linhas o que antes eram os valores de colunas """

def main(df):

    inpc_ano = filterDataFrame(df, 20)
    grafPlot(inpc_ano, ['artigos de residencia'], 'line', 'INPC 2020 ARTIGOS DE RESIDÊNCIA')

if __name__ == '__main__':
    df = pd.read_csv("./inpc_2015-2020.csv", index_col=0)
    """ Abre o arquivo csv e define como index os valores da primeira coluna """
    main(df)
